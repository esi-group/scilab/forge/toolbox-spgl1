// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================
load('lasso.dat');
tau  = %pi;
y    = spg_lasso(A,b,tau);
assert_checkalmostequal(x,y);

load('bp.dat');
opts = spgSetParms('verbosity',1);
y    = spg_bp(A, b, opts);
assert_checkalmostequal(x,y);

load('bpdn.dat');
sigma = 0.10;
opts  = spgSetParms('verbosity',1);
y     = spg_bpdn(A, b, sigma, opts);
assert_checkalmostequal(x,y);
//=================================
