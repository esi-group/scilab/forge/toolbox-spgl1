function [sp] = spgl1_sparse(i,j,s,m,n)
  nnz     = length(s);
  if ( length(i) ~= nnz ) then
    error('spgl1_sparse : i and s must have same length');
  end
  if ( length(j) ~= nnz ) then
    error('spgl1_sparse : j and s must have same length');
  end
  mn      = [m n];
  ij      = zeros(nnz,2);
  ij(:,1) = i';
  ij(:,2) = j';
  sp      = sparse( ij, s, mn );
endfunction
