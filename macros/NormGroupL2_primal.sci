function p = NormGroupL2_primal(groups,x,weights)

if isreal(x)
   p = sum(weights.*sqrt(sum(groups * x.^2,"c")));
else
   p = sum(weights.*sqrt(sum(groups * abs(x).^2,"c")));
end

endfunction
//
