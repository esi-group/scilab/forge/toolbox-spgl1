function [sp] = spgl1_spdiags(B,d,m,n)
  if ( d ~= 0 ) then
    printf("d=%e\n",d);
    error('spgl1_spdiags only implemented for d=0');
  end
  if ( m ~= n ) then
    printf("m=%d - n=%d\n",m,n);
    error('spgl1_spdiags only implemented for m=n');
  end
  if ( (size(B,1) ~= n) | (size(B,2) ~= 1 ) ) then
    printf("m=%d - n=%d\n",m,n);
    printf("size(B,1)=%d - size(B,2)=%d\n",size(B,1),size(B,2));
    error('spgl1_spdiags only implemented for B a vector n x 1');
  end
  ij      = zeros(n,2);
  ij(:,1) = [1:n]';
  ij(:,2) = [1:n]';
  v       = zeros(n,1);
  v(:)    = B(:,1);
  mn      = [m n];
  sp      = sparse( ij, v, mn);
endfunction
