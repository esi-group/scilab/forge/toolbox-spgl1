function [C,ia,ic] = spgl1_unique(A)
  [C,ia]  = unique(A);
  [nb,ic] = members(A,C);
endfunction
