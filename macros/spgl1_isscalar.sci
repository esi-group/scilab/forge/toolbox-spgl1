function [b] = spgl1_isscalar(A)
   b = sum( length(A) ) == 1
endfunction
