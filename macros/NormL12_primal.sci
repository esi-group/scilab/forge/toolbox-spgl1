function p = NormL12_primal(g,x,weights)

m = round(length(x) / g); n = g;

if isreal(x)
   p = sum(weights.*sqrt(sum(matrix(x,m,n).^2,"c")));
else
   p = sum(weights.*sqrt(sum(abs(matrix(x,m,n)).^2,"c")));
end

endfunction
//
