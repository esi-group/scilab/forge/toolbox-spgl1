function [x, itn] = oneProjectorMex(b,d,tau)
// [x, itn] = oneProjectorMex(b,d,tau) 
// Return the orthogonal projection of the vector b >=0 onto the
// (weighted) L1 ball. In case vector d is specified, matrix D is
// defined as diag(d), otherwise the identity matrix is used.
//
// On exit,
// x      solves   minimize  ||b-x||_2  st  ||Dx||_1 <= tau.
// itn    is the number of elements of b that were thresholded.
//
// See also spgl1, oneProjector.

//   oneProjectorMex.m
//   $Id: oneProjectorMex.m 1200 2008-11-21 19:58:28Z mpf $
//
//   ----------------------------------------------------------------------
//   This file is part of SPGL1 (Spectral Projected Gradient for L1).
//
//   Copyright (C) 2007 Ewout van den Berg and Michael P. Friedlander,
//   Department of Computer Science, University of British Columbia, Canada.
//   All rights reserved. E-mail: <{ewout78,mpf}@cs.ubc.ca>.
//
//   SPGL1 is free software; you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as
//   published by the Free Software Foundation; either version 2.1 of the
//   License, or (at your option) any later version.
//
//   SPGL1 is distributed in the hope that it will be useful, but WITHOUT
//   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
//   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General
//   Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with SPGL1; if not, write to the Free Software
//   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
//   USA
//   ----------------------------------------------------------------------

if argn(2) < 3
   tau = d;
   d   = 1;
end

if spgl1_isscalar(d)
  [x,itn] = oneProjectorMex_I(b,tau/abs(d));
else
  [x,itn] = oneProjectorMex_D(b,d,tau);
end

endfunction // function oneProjectorMex

