function options = spgSetParms(varargin)
//SPGSETPARMS  Set options for SPGL1
//
//   options = spgSetParms('param1',val1,'param2',val2,...) creates an
//   options structure in which the named parameters have the specified
//   values.  Unspecified parameters are empty and their default
//   values are used.
//   
//   spgSetParms with no input arguments and no output arguments
//   displays all parameter names and their possible values.
//
//   options = spgSetParms (with no input arguments) creates an options
//   structure where all the fields are empty.
//
//   spgSetParms.m
//   $Id: spgSetParms.m 1407 2009-06-30 20:00:54Z ewout78 $

// Print out possible values of properties.
nargin  = argn(2);
nargout = argn(1);

if nargin == 0 & nargout == 0
   mtlb_fprintf(' Default parameters for l1Set.m:\n');
   mtlb_fprintf('        fid : [ positive integer        |     1 ]\n');
   mtlb_fprintf('  verbosity : [ integer: 1, 2, or 3     |     3 ]\n');
   mtlb_fprintf(' iterations : [ positive integer        |  10*m ]\n');
   mtlb_fprintf('  nPrevVals : [ positive integer        |    10 ]\n');
   mtlb_fprintf('      bpTol : [ positive scalar         | 1e-06 ]\n');
   mtlb_fprintf('      lsTol : [ positive scalar         | 1e-06 ]\n');
   mtlb_fprintf('     optTol : [ positive scalar         | 1e-04 ]\n');
   mtlb_fprintf('     decTol : [ positive scalar         | 1e-04 ]\n');   
   mtlb_fprintf('    stepMin : [ positive scalar         | 1e-16 ]\n');
   mtlb_fprintf('    stepMax : [ positive scalar         | 1e+05 ]\n');
   mtlb_fprintf(' rootMethod : [ 1=linear, 2=quadratic   |     2 ]\n');
   mtlb_fprintf('activeSetIt : [ positive integer        |   Inf ]\n');
   mtlb_fprintf('subspaceMin : [ 0=no, 1=yes             |     0 ]\n');
   mtlb_fprintf('  iscomplex : [ 0=no, 1=yes, NaN=auto   |   NaN ]\n');
   mtlb_fprintf('  maxMatvec : [ positive integer        |   Inf ]\n');
   mtlb_fprintf('    weights : [ vector                  |     1 ]\n');
   mtlb_fprintf('    project : [ projection function     |    @()]\n');
   mtlb_fprintf('primal_norm : [ primal norm eval fun    |    @()]\n');
   mtlb_fprintf('  dual_norm : [ dual norm eval fun      |    @()]\n');
   mtlb_fprintf('\n');
   return;
end

Names = [
    'fid               '
    'verbosity         '
    'iterations        '
    'nPrevVals         '
    'bpTol             '
    'lsTol             '
    'optTol            '
    'decTol            '
    'stepMin           '
    'stepMax           '
    'rootMethod        '
    'activeSetIt       '
    'subspaceMin       '
    'iscomplex         '
    'maxMatvec         '
    'weights           '
    'project           '
    'primal_norm       '
    'dual_norm         '
	];
[m,n] = size(Names);
names = convstr(Names,"l");

// Combine all leading options structures o1, o2, ... in l1Set(o1,o2,...).
options = [];
for j = 1:m
   execstr(['options.'+Names(j,:)+'= [];']);
end
i = 1;
while i <= nargin
   arg = varargin(i);
   if (type(arg)==10), break; end // ischar
   if ~isempty(arg)                      // [] is a valid options argument
       if ~(typeof(arg)=='st')
          error(msprintf(['Expected argument %d to be a string parameter name '+ ...
               'or an options structure\ncreated with OPTIMSET.'], i));
      end
      for j = 1:m
          if or((fieldnames(arg)==stripblanks(Names(j,:))))
             //mfprintf(%io(2),'val = arg.'+Names(j,:)+';\n')
             execstr(['val = arg.'+Names(j,:)+';']);
          else
             val = [];
          end
          //mfprintf(%io(2),'type of val : %d\n',type(val));
          if ( (type(val)==1 & ~isempty(val)) | type(val) <> 1 )
             execstr(['options.'+Names(j,:)+'= val;']);
         end
      end
   end
   i = i + 1;
end

// A finite state machine to parse name-value pairs.
if modulo(nargin-i+1,2) ~= 0
   error('Arguments must occur in name-value pairs.');
end
expectval = 0;                          // start expecting a name, not a value
while i <= nargin
   arg = varargin(i);
   
   if ~expectval
      if ~(type(arg) == 10) // ~ischar
         error(sprintf('Expected argument %d to be a string parameter name.', i));
      end
      
      lowArg = convstr(arg,"l");
      j = grep(names,lowArg); // j = strmatch(lowArg,names);
      if isempty(j)                       // if no matches
         error(sprintf('Unrecognized parameter name ''%s''.', arg));
      elseif length(j) > 1                // if more than one match
         // Check for any exact matches (in case any names are subsets of others)
         k = grep(names,lowArg); // k = strmatch(lowArg,names,'exact');
         if length(k) == 1
            j = k;
         else
            msg = sprintf('Ambiguous parameter name ''%s'' ', arg);
            msg = [msg '(' stripblanks(Names(j(1),:))];
            for k = j(2:length(j))'
               msg = [msg ', ' stripblanks(Names(k,:))];
            end
            msg = sprintf('%s).', msg);
            error(msg);
         end
      end
      expectval = 1;                      // we expect a value next
      
   else
      execstr(['options.'+Names(j,:)+'= arg;']);
      expectval = 0;
      
   end
   i = i + 1;
end

if expectval
   error(msprintf('Expected value for parameter ''%s''.'+arg));
end

endfunction
