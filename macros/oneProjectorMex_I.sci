// ----------------------------------------------------------------------
function [x,itn] = oneProjectorMex_I(b,tau)
// ----------------------------------------------------------------------

   // Initialization
   n     = length(b);
   x     = zeros(n,1);
   bNorm = norm(b,1);

   // Check for quick exit.
   if (tau >= bNorm), x = b; itn = 0; return; end
   if (tau <  %eps  ),        itn = 0; return; end

   // Preprocessing (b is assumed to be >= 0)
   [b,idx] = gsort(b,'g','d'); // Descending.

   csb       = -tau;
   alphaPrev = 0;
   for j= 1:n
      csb       = csb + b(j);
      alpha     = csb / j;
   
      // We are done as soon as the constraint can be satisfied
      // without exceeding the current minimum value of b
      if alpha >= b(j)
         break;
      end
   
      alphaPrev = alpha;
   end

   // Set the solution by applying soft-thresholding with
   // the previous value of alpha
   x(idx) = max(0,b - alphaPrev);

   // Set number of iterations
   itn = j;
endfunction

