function [b] = spgl1_isnumeric(A)
   b = (or(type(A)==[1 5 8]));
endfunction
