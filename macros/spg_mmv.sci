function [x,r,g,info] = spg_mmv( A0, B0, sigma, options )
//SPG_MMV  Solve multi-measurement basis pursuit denoise (BPDN)
//
// Calling Sequence
// [x, r, g, info] = spg_mmv(A0, B0, sigma, options)
//
// Parameters
// A0      : (IN) is an m-by-n matrix, explicit or an operator.
// B0      : (IN) is an m-by-g matrix.
// sigma   : (IN) is a nonnegative scalar (noise); see (BPDN)
// options : (IN) is a structure of options from spgSetParms
// x       : (OUT) is a solution of the problem
// r       : (OUT) is the residual, r = b - Ax
// g       : (OUT) is the gradient, g = -A'r
// info    : (OUT) is a structure with output information
//
// Description
//
//   (BPDN)  minimize  ||X||_1,2  subject to  ||A X - B||_2,2 <= SIGMA,
//
//   where A is an M-by-N matrix, B is an M-by-G matrix, and SIGMA is a
//   nonnegative scalar.  In all cases below, A can be an explicit M-by-N
//   matrix or matrix-like object for which the operations  A*x  and  A'*y
//   are defined (i.e., matrix-vector multiplication with A and its
//   adjoint.)
//
//   Also, A can be a function handle that points to a function with the
//   signature
//
//   v = A(w,mode)   which returns  v = A *w  if mode == 1;
//                                  v = A'*w  if mode == 2. 
//   
//   X = SPG_MMV(A,B,SIGMA) solves the BPDN problem.  If SIGMA=0 or
//   SIGMA=[], then the basis pursuit (BP) problem is solved; i.e., the
//   constraints in the BPDN problem are taken as AX=B.
//
//   X = SPG_MMV(A,B,SIGMA,OPTIONS) specifies options that are set using
//   SPGSETPARMS.
//
//   [X,R,G,INFO] = SPG_BPDN(A,B,SIGMA,OPTIONS) additionally returns the
//   residual R = B - A*X, the objective gradient G = A'*R, and an INFO
//   structure.  (See SPGL1 for a description of this last output argument.)
//
// Authors
// for the original Matlab implementation :
//  Ewout van den Berg (ewout78@cs.ubc.ca)
//  Michael P. Friedlander (mpf@cs.ubc.ca)
//    Scientific Computing Laboratory (SCL)
//    University of British Columbia, Canada
// for the Scilab port :
//  François Béreux (francois.bereux@gmail.com)

// --------------------------------------
// Original header below
// --------------------------------------
//SPG_MMV  Solve multi-measurement basis pursuit denoise (BPDN)
//
//   SPG_MMV is designed to solve the basis pursuit denoise problem
//
//   (BPDN)  minimize  ||X||_1,2  subject to  ||A X - B||_2,2 <= SIGMA,
//
//   where A is an M-by-N matrix, B is an M-by-G matrix, and SIGMA is a
//   nonnegative scalar.  In all cases below, A can be an explicit M-by-N
//   matrix or matrix-like object for which the operations  A*x  and  A'*y
//   are defined (i.e., matrix-vector multiplication with A and its
//   adjoint.)
//
//   Also, A can be a function handle that points to a function with the
//   signature
//
//   v = A(w,mode)   which returns  v = A *w  if mode == 1;
//                                  v = A'*w  if mode == 2. 
//   
//   X = SPG_MMV(A,B,SIGMA) solves the BPDN problem.  If SIGMA=0 or
//   SIGMA=[], then the basis pursuit (BP) problem is solved; i.e., the
//   constraints in the BPDN problem are taken as AX=B.
//
//   X = SPG_MMV(A,B,SIGMA,OPTIONS) specifies options that are set using
//   SPGSETPARMS.
//
//   [X,R,G,INFO] = SPG_BPDN(A,B,SIGMA,OPTIONS) additionally returns the
//   residual R = B - A*X, the objective gradient G = A'*R, and an INFO
//   structure.  (See SPGL1 for a description of this last output argument.)
//
//   See also spgl1, spgSetParms, spg_bp, spg_lasso.

//   Copyright 2008, Ewout van den Berg and Michael P. Friedlander
//   http://www.cs.ubc.ca/labs/scl/spgl1
//   $Id$

if ~mtlb_exist('options','var'), options = []; end
if ~mtlb_exist('sigma','var'), sigma = 0; end
if ~mtlb_exist('B0','var') | isempty(B0)
    error('Second argument cannot be empty.');
end
if ~mtlb_exist('A0','var') then
    error('First argument cannot be empty.');
else
  if ( type(A0) ~= 13 ) then
    if isempty(A0) then
      error('First argument cannot be empty.');
    end
  end
end

groups = size(B0,2);
if type(A0) == 13
   y = A0(B0(:,1),2); m0 = size(B0,1); n0 = length(y);
   function [yy] = AA( x, mode )
     yy = blockDiagonalImplicit(A0,m0,n0,groups,x,mode);
   endfunction
else
   m0 = size(A0,1); n0 = size(A0,2);
   function [yy] = AA( x, mode )
     yy = blockDiagonalExplicit(A0,m0,n0,groups,x,mode);
   endfunction
end

// Set projection specific functions
// inline functions
function [xx] = NormL12_project_partial( x, weight, tau )
   xx = NormL12_project(groups,x,weight,tau);
endfunction

function [xx] = NormL12_primal_partial( x, weight )
   xx = NormL12_primal(groups,x,weight);
endfunction

function [xx] = NormL12_dual_partial( x, weight )
   xx = NormL12_dual(groups,x,weight);
endfunction

options.project     = NormL12_project_partial;
options.primal_norm = NormL12_primal_partial;
options.dual_norm   = NormL12_dual_partial;

tau = 0;
x0  = [];
[x,r,g,info] = spgl1(AA,B(:),tau,sigma,x0,options);

n = round(length(x) / groups);
m = size(B0,1);
x = matrix(x,n,groups);
y = matrix(r,m,groups);
g = matrix(g,n,groups);

endfunction

function y = blockDiagonalImplicit(A0,m,n,g,x,mode)

if mode == 1
   y = zeros(m*g,1);
   for i=1:g
      y(1+(i-1)*m:i*m) = A0(x(1+(i-1)*n:i*n),mode);
   end
else
   y = zeros(n*g,1);
   for i=1:g
      y(1+(i-1)*n:i*n) = A0(x(1+(i-1)*m:i*m),mode);
   end   
end

endfunction


function y = blockDiagonalExplicit(A0,m,n,g,x,mode)
if mode == 1
   y = A0 * matrix(x,n,g);
   y = y(:);
else
   x = matrix(x,m,g);
   y = (x' * A0)';
   y = y(:);
end

endfunction
//
