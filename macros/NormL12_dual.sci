function d = NormL12_dual(g,x,weights)

m = round(length(x) / g); n = g;

if isreal(x)
   d = norm(sqrt(sum(matrix(x,m,n).^2,"c"))./weights,%inf);
else
   d = norm(sqrt(sum(abs(matrix(x,m,n)).^2,"c"))./weights,%inf);
end

endfunction
//  
